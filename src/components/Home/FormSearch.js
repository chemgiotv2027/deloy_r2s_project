import dayjs from "dayjs";
const FormSearch = () => {
    const [formSearch] = Form.useForm();
    const [searhRaise, setSearchRaise] = useState(false);
    const dateFormat = "DD/MM/YYYY";

    const handleSubmit = (values) => {
        const { code } = values;
        setSearchRecord(deliveryOrderState?.deliveryOrder?.items.filter((item) => item.code === code));
    };
    const handleClear = () => {
        formSearch.resetFields();
    };
    const showModalExport = () => {
        setOpen(true);
    };
    const CreateOrder = () => {
        form.resetFields();
        setIsModalOpen(true);
        setCreateForm(true);
    };
    return (
        <Form form={formSearch} onFinish={handleSubmit} name="formSearch">
            <div className="search">
                <Row gutter={12}>
                    <Col span={6}>
                        <Form.Item label="Mã code" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="code">
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label="Trạng thái đơn hàng" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="status">
                            <Select placeholder="Chọn Trạng thái đơn hàng">
                                <Option value="option1">Đơn hàng</Option>
                                <Option value="option2">Đơn hàng đã đi</Option>
                                <Option value="option3">Đơn hàng tồn kho</Option>
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label="Hình thức thanh toán" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="paymentType1">
                            <Select placeholder="Chọn hình thức thanh toán">
                                <Option value="option1">TTS</Option>
                                <Option value="option2">TĐN</Option>
                                <Option value="option3">ĐTT</Option>
                                <Option value="option3">Khác</Option>
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label="Đơn hàng hoàn tất" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="isDone">
                            <Select placeholder="Chọn đơn hàng đã hoàn tất hay chưa">
                                <Option value="option1">Đơn hàng đã hoàn tất</Option>
                                <Option value="option2">Đơn hàng chưa hoàn tất</Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>

                {searhRaise && (
                    <Row gutter={12}>
                        <Col span={6}>
                            <Form.Item label=" Nhân viên kinh doanh" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="status">
                                <Select placeholder="Chọn nhân viên kinh doanh">
                                    <Option value="option1">Đơn hàng</Option>
                                    <Option value="option2">Đơn hàng đã đi</Option>
                                    <Option value="option3">Đơn hàng tồn kho</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={6}>
                            <Form.Item label="Từ ngày - Đến ngày" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="code">
                                <RangePicker defaultValue={[dayjs("01/01/2015", dateFormat), dayjs("06/07/2023", dateFormat)]} format={dateFormat} />
                            </Form.Item>
                        </Col>
                        <Col span={6}>
                            <Form.Item label="Địa điểm nhận hàng" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="fromAddress">
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col span={6}>
                            <Form.Item label="Địa điểm giao hàng" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="toAddress">
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col span={6}>
                            <Form.Item label="Tên khách hàng" labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} name="code">
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                )}

                <div className="choose">
                    <Space className="timkiem">
                        <Button onClick={handleClear}>Clear</Button>
                        <Button htmlType="submit">Tìm kiếm</Button>
                        <Button onClick={showModalExport}>Export Excel</Button>
                        <Button onClick={CreateOrder}>Tạo mới đơn</Button>
                        <Checkbox onChange={() => setSearchRaise((searhRaise) => !searhRaise)} value={searhRaise}>
                            Tìm kiếm nâng cao
                        </Checkbox>
                    </Space>
                    <Radio.Group onChange={onChange} value={value}>
                        <Radio value={true}>Rút gọn</Radio>
                        <Radio value={false}>Toàn bộ cột</Radio>
                    </Radio.Group>
                </div>
            </div>
        </Form>
    );
};
